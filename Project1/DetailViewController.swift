//
//  DetailViewController.swift
//  Project1
//
//  Created by Роман Хоменко on 27.03.2022.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    var selectedImage: String?
    var selectedImageIndex: String?
    var numberOfImages: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        assert(selectedImage?.isEmpty == false, "selected image path is empty")
        
        title = "Picture \(selectedImageIndex ?? "") of \(numberOfImages ?? "")"
        navigationItem.largeTitleDisplayMode = .never

        if let imageToLoad = selectedImage {
            imageView.image = UIImage(named: imageToLoad)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.hidesBarsOnTap = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        navigationController?.hidesBarsOnTap = false
    }
}
