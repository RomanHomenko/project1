//
//  ImageCell.swift
//  Project1
//
//  Created by Роман Хоменко on 06.04.2022.
//

import UIKit

class ImageCell: UICollectionViewCell {
    @IBOutlet var stormImage: UIImageView!
    @IBOutlet var imageNameLabel: UILabel!
}
