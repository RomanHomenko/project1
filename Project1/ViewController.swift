//
//  ViewController.swift
//  Project1
//
//  Created by Роман Хоменко on 27.03.2022.
//

import UIKit

class ViewController: UICollectionViewController {
    var storms = [Storm]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        settingView()
        
        let defaults = UserDefaults.standard
        if let savedStorms = defaults.object(forKey: "storms") as? Data {
            let jsonDecoder = JSONDecoder()
            
            do {
                storms = try jsonDecoder.decode([Storm].self, from: savedStorms)
            } catch let error {
                print(error)
            }
        }
        
        performSelector(inBackground: #selector(fetchImagesFromBundle), with: nil)
    }
}

extension ViewController {
    func settingView() {
        title = "Storm Viewer"
        navigationController?.navigationBar.prefersLargeTitles = true
    }
}

extension ViewController {
    @objc func fetchImagesFromBundle() {
        
        if storms.isEmpty {
            let fileManager = FileManager.default
            let path = Bundle.main.resourcePath!
            let items = try! fileManager.contentsOfDirectory(atPath: path)
            
            for item in items {
                if item.hasPrefix("nssl") {
                    
                    storms.append(Storm(name: item, image: item))
                }
            }
            
            storms = storms.sorted { $0.name < $1.name }
            
            // Solve problem with purple error
            DispatchQueue.main.async { [weak self] in
                self?.collectionView.reloadData()
            }
        }
        
        // MARK: - There is an error. Code works without crashes but puple error appears
        
//        tableView.performSelector(onMainThread: #selector(UITableView.reloadData), with: nil, waitUntilDone: false)
    }
}

extension ViewController {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return storms.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Image", for: indexPath) as? ImageCell else {
            fatalError("Unable dequeue ImageCell")
        }
        
        let picture = storms[indexPath.item]
        
        cell.imageNameLabel.text = "\(picture.tapCount)"
        cell.stormImage.image = UIImage(named: picture.image)
        
        cell.stormImage.layer.borderColor = UIColor(white: 0, alpha: 0.3).cgColor
        cell.stormImage.layer.borderWidth = 2
        cell.stormImage.layer.cornerRadius = 3
        cell.layer.borderColor = UIColor(white: 0, alpha: 0.3).cgColor
        cell.layer.borderWidth = 2
        cell.layer.cornerRadius = 7
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // test breakpoint with identifier "Bad"
            if let vc = storyboard?.instantiateViewController(withIdentifier: "Detail") as? DetailViewController {
                vc.selectedImage = storms[indexPath.item].name
                vc.numberOfImages = String(storms.count)
                vc.selectedImageIndex = String(indexPath.row + 1)
                storms[indexPath.item].tapCount += 1
                save()
                collectionView.reloadData()

                navigationController?.pushViewController(vc, animated: true)
            }
    }
}


extension ViewController {
    func save() {
        let jsonEncoder = JSONEncoder()
        
        if let savedData = try? jsonEncoder.encode(storms) {
            let defaults = UserDefaults.standard
            defaults.set(savedData, forKey: "storms")
        } else {
            print("Faild to save storms")
        }
    }
}


// MARK: - Settings for UITableViewController

//extension ViewController {
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return pictures.count
//    }
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "Picture", for: indexPath)
//        cell.textLabel?.text = pictures[indexPath.row]
//
//        return cell
//    }
//
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if let vc = storyboard?.instantiateViewController(withIdentifier: "Detail") as? DetailViewController {
//            vc.selectedImage = pictures[indexPath.row]
//            vc.numberOfImages = String(pictures.count)
//            vc.selectedImageIndex = String(indexPath.row + 1)
//
//            navigationController?.pushViewController(vc, animated: true)
//        }
//    }
//}
