//
//  Storm.swift
//  Project1
//
//  Created by Роман Хоменко on 06.04.2022.
//

import Foundation

class Storm: Codable {
    var name: String
    var image: String
    var tapCount: Int = 0
    
    init(name: String, image: String) {
        self.name = name
        self.image = image
    }
}
